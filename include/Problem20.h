#ifndef Problem20_H
#define Problem20_H

#include <ProblemSolver.h>


class Problem20 : public ProblemSolver
{
    public:
        Problem20(unsigned int fac);
        virtual ~Problem20();
        void resolve();
        void printResult();
    protected:
    private:
        unsigned int m_fac;
};

#endif // Problem20_H
