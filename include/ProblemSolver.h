#ifndef PROBLEMSOLVER_H
#define PROBLEMSOLVER_H
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string>
#include <algorithm>
#include <memory.h>
using namespace std;


class ProblemSolver
{
    public:
        ProblemSolver(unsigned int result_size);
        virtual ~ProblemSolver();
        virtual void resolve() = 0;
        virtual void printResult() = 0;
    protected:
        char* result;
};

#endif // PROBLEMSOLVER_H
