#ifndef PROBLEM1_H
#define PROBLEM1_H

#include <ProblemSolver.h>


class Problem1 : public ProblemSolver
{
    public:
        Problem1(unsigned int limit);
        virtual ~Problem1();
        void resolve();
        void printResult();
    protected:
    private:
        unsigned int m_limit;
};

#endif // PROBLEM1_H
