#ifndef Problem2_H
#define Problem2_H

#include <ProblemSolver.h>


class Problem2 : public ProblemSolver
{
    public:
        Problem2(unsigned long long limit);
        virtual ~Problem2();
        void resolve();
        void printResult();
    protected:
    private:
        unsigned long long m_limit;
};

#endif // Problem2_H
