#include "Problem1.h"
Problem1::Problem1(unsigned int limit) : ProblemSolver(sizeof(unsigned int))
{
   m_limit = limit;
}

Problem1::~Problem1()
{
    //dtor
}

void Problem1::resolve()
{
    unsigned int s = 0;
    for(register int i = 0; i < m_limit; ++i)
    {
        if(i%3==0 || i%5==0)
            s += i;
    }
    memcpy(result,&s,sizeof(s));
}
void Problem1::printResult()
{
    cout << "the sum of all the multiples of 3 or 5 below " << m_limit << " is " << *(unsigned int*)result << endl;
}



