#include "Problem20.h"
Problem20::Problem20(unsigned int fac) : ProblemSolver(sizeof(unsigned long long))
{
   m_fac = fac;
}

Problem20::~Problem20()
{
    //dtor
}

void Problem20::resolve()
{
    unsigned long long s = 0;
    for(unsigned int i = m_fac; i > 0; --i)
    {
        s += i;
    }
    memcpy(result,&s,sizeof(s));
}
void Problem20::printResult()
{
    cout << "The sum of the digits in the number " << m_fac << "! is " << *(unsigned long long*)result << endl;
}



