#include "Problem2.h"
Problem2::Problem2(unsigned long long limit) : ProblemSolver(sizeof(unsigned long long))
{
   m_limit = limit;
}

Problem2::~Problem2()
{
    //dtor
}

void Problem2::resolve()
{
    unsigned long long s = 0;
    unsigned long long c = 0, p = 0, r = 1;
    while(r < m_limit)
    {
        if(r%2==0)
            s += r;
        p = c;
        c = r;
        r = c+p;
    }
    memcpy(result,&s,sizeof(s));
}
void Problem2::printResult()
{
    cout << "The sum of the even-valued terms in the Fibonacci sequence whose values do not exceed " << m_limit << " is " << *(unsigned long long*)result << endl;
}



