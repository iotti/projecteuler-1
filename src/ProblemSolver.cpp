#include "ProblemSolver.h"

ProblemSolver::ProblemSolver(unsigned int result_size)
{
    result = new char[result_size];
    memset(result,0,result_size);
}
ProblemSolver::~ProblemSolver()
{
    delete result;
    result = NULL;
}
