#include <iostream>
#include "SolverIncluder.h"
using namespace std;



int main()
{
    ProblemSolver *p = new Problem1(1000);
    p->resolve();
    p->printResult();
    delete p;

    p = new Problem2(4000000);
    p->resolve();
    p->printResult();
    delete p;

    p = new Problem20(100);
    p->resolve();
    p->printResult();
    delete p;

    p = NULL;
    return 0;
}


